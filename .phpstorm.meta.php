<?php

namespace PHPSTORM_META {

    use Turahe\Wallet\Interfaces\Mathable;
    use Turahe\Wallet\Interfaces\Rateable;
    use Turahe\Wallet\Interfaces\Storable;
    use Turahe\Wallet\Models\Transaction;
    use Turahe\Wallet\Models\Transfer;
    use Turahe\Wallet\Models\Wallet;
    use Turahe\Wallet\Objects\Bring;
    use Turahe\Wallet\Objects\Cart;
    use Turahe\Wallet\Objects\EmptyLock;
    use Turahe\Wallet\Objects\Operation;
    use Turahe\Wallet\Services\CommonService;
    use Turahe\Wallet\Services\ExchangeService;
    use Turahe\Wallet\Services\WalletService;

    override(\app(0), map([
        Cart::class => Cart::class,
        Bring::class => Bring::class,
        Operation::class => Operation::class,
        EmptyLock::class => EmptyLock::class,
        ExchangeService::class => ExchangeService::class,
        CommonService::class => CommonService::class,
        WalletService::class => WalletService::class,
        Wallet::class => Wallet::class,
        Transfer::class => Transfer::class,
        Transaction::class => Transaction::class,
        Mathable::class => Mathable::class,
        Rateable::class => Rateable::class,
        Storable::class => Storable::class,
    ]));

}
