<?php

namespace Turahe\Wallet\Exceptions;

class BalanceIsEmpty extends InsufficientFunds
{
}
