<?php

namespace Turahe\Wallet\Exceptions;

use InvalidArgumentException;

class ConfirmedInvalid extends InvalidArgumentException
{
}
