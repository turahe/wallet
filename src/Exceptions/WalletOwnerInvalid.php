<?php

namespace Turahe\Wallet\Exceptions;

use InvalidArgumentException;

class WalletOwnerInvalid extends InvalidArgumentException
{
}
