<?php

namespace Turahe\Wallet\Exceptions;

use InvalidArgumentException;

class AmountInvalid extends InvalidArgumentException
{
}
