<?php

namespace Turahe\Wallet\Exceptions;

use LogicException;

class InsufficientFunds extends LogicException
{
}
