<?php

namespace Turahe\Wallet\Models;

use App\Traits\HasMetadata;
use Turahe\Wallet\Interfaces\Confirmable;
use Turahe\Wallet\Interfaces\Customer;
use Turahe\Wallet\Interfaces\Exchangeable;
use Turahe\Wallet\Interfaces\WalletFloat;
use Turahe\Wallet\Services\WalletService;
use Turahe\Wallet\Traits\CanConfirm;
use Turahe\Wallet\Traits\CanExchange;
use Turahe\Wallet\Traits\CanPayFloat;
use Turahe\Wallet\Traits\HasGift;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Str;

/**
 * Turahe\Wallet\Models\Wallet
 *
 * @property int $id
 * @property string $holder_type
 * @property int $holder_id
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property int|float|string $balance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read int|float|string $balance_float
 * @property-read string $currency
 * @property-read Model|\Eloquent $holder
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Metadata[] $meta
 * @property-read int|null $meta_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Turahe\Wallet\Models\Transaction[] $transactions
 * @property-read int|null $transactions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Turahe\Wallet\Models\Transfer[] $transfers
 * @property-read int|null $transfers_count
 * @property-read Wallet $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet query()
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereHolderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereHolderType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Wallet extends Model implements Customer, WalletFloat, Confirmable, Exchangeable
{
    use HasMetadata;
    use CanConfirm;
    use CanExchange;
    use CanPayFloat;
    use HasGift;

    /**
     * @var array
     */
    protected $fillable = [
        'holder_type',
        'holder_id',
        'name',
        'slug',
        'description',
        'meta',
        'balance',
        'decimal_places',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'decimal_places' => 'int',
        'meta' => 'json',
    ];

    /**
     * @param string $name
     * @return void
     */
    public function setNameAttribute(string $name): void
    {
        $this->attributes['name'] = $name;

        /*
         * Must be updated only if the model does not exist
         *  or the slug is empty.
         */
        if (! $this->exists && ! array_key_exists('slug', $this->attributes)) {
            $this->attributes['slug'] = Str::slug($name);
        }
    }

    /**
     * Under ideal conditions, you will never need a method.
     * Needed to deal with out-of-sync.
     *
     * @return bool
     */
    public function refreshBalance(): bool
    {
        return app(WalletService::class)->refresh($this);
    }

    /**
     * The method adjusts the balance by adding an additional transaction.
     * Used wisely, it can lead to serious problems.
     *
     * @return bool
     */
    public function adjustmentBalance(): bool
    {
        try {
            app(WalletService::class)->adjustment($this);

            return true;
        } catch (\Throwable $throwable) {
            return false;
        }
    }

    /**
     * @return float|int
     */
    public function getAvailableBalance()
    {
        return $this->transactions()
            ->where('wallet_id', $this->getKey())
            ->where('confirmed', true)
            ->sum('amount');
    }

    /**
     * @return MorphTo
     */
    public function holder(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    public function getCurrencyAttribute(): string
    {
        $currencies = config('wallet.currencies', []);

        return $currencies[$this->slug] ??
            $this->meta['currency'] ??
            Str::upper($this->slug);
    }
}
