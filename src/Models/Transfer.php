<?php

namespace Turahe\Wallet\Models;

use App\Traits\HasMetadata;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Turahe\Wallet\Models\Transfer
 *
 * @property int $id
 * @property string $from_type
 * @property int $from_id
 * @property string $to_type
 * @property int $to_id
 * @property int $deposit_id
 * @property int $withdraw_id
 * @property string $uuid
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Turahe\Wallet\Models\Transaction $deposit
 * @property-read Model|\Eloquent $from
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Metadata[] $meta
 * @property-read int|null $meta_count
 * @property-read Model|\Eloquent $to
 * @property-read \Turahe\Wallet\Models\Transaction $withdraw
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereDepositId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereFromId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereFromType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereToType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transfer whereWithdrawId($value)
 * @mixin \Eloquent
 */
class Transfer extends Model
{
    use HasMetadata;
    public const STATUS_EXCHANGE = 'exchange';
    public const STATUS_TRANSFER = 'transfer';
    public const STATUS_PAID = 'paid';
    public const STATUS_REFUND = 'refund';
    public const STATUS_GIFT = 'gift';

    /**
     * @var array
     */
    protected $fillable = [
        'status',
        'discount',
        'deposit_id',
        'withdraw_id',
        'from_type',
        'from_id',
        'to_type',
        'to_id',
        'uuid',
        'fee',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'deposit_id' => 'int',
        'withdraw_id' => 'int',
    ];


    /**
     * @return MorphTo
     */
    public function from(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return MorphTo
     */
    public function to(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return BelongsTo
     */
    public function deposit(): BelongsTo
    {
        return $this->belongsTo(Transaction::class, 'deposit_id');
    }

    /**
     * @return BelongsTo
     */
    public function withdraw(): BelongsTo
    {
        return $this->belongsTo(Transaction::class, 'withdraw_id');
    }
}
