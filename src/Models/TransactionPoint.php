<?php

namespace Turahe\Wallet\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Turahe\Wallet\Models\TransactionPoint.
 *
 * @property int $id
 * @property string $message
 * @property string $pointable_type
 * @property int $pointable_id
 * @property float $amount
 * @property float $current
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Model|\Eloquent $pointable
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint query()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint whereCurrent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint wherePointableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint wherePointableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $hash
 * @property string|null $prevhash
 * @property-read mixed $transaction_id
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionPoint wherePrevhash($value)
 */
class TransactionPoint extends Model
{
    /**
     * @var string
     */
    protected $table = 'transaction_points';

    protected $fillable = [
        'message', 'amount', 'current',
    ];

    public function pointable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @param Model $pointable
     *
     * @return static
     */
    public function getCurrentPoints(Model $pointable)
    {
        $currentPoint = self::
         where('pointable_id', $pointable->id)
         ->where('pointable_type', $pointable->getMorphClass())
         ->orderBy('created_at', 'desc')
         ->pluck('current')->first();

        if (! $currentPoint) {
            $currentPoint = 0.0;
        }

        return $currentPoint;
    }

    /**
     * @param Model $pointable
     * @param $amount
     * @param $message
     * @return static
     */
    public function addTransaction(Model $pointable, $amount, $message):self
    {
        $point = $this->getCurrentPoints($pointable) + $amount;

        $Blockchain = new \flavienbwk\BlockchainPHP\Blockchain();
        $block = $Blockchain->addBlock(storage_path('blockchain.dat'), json_encode([
            'amount' => $amount,
            'current' => $point,
            'message' => $message,
        ]));

        $transaction = new static();
        $transaction->amount = $amount;

        $transaction->current = $point;

        $transaction->message = $message;
        $transaction->prevhash = $block->getPrevHash();  // Returns the hash (SHA256) of the block added before this one.;
        $transaction->hash = $block->getHash();

        $pointable->transactionPoints()->save($transaction);

        return $transaction;
    }

    public function getTransactionIdAttribute()
    {
        return 'TX-'.$this->id.'-'.strtotime($this->created_at);
    }
}
