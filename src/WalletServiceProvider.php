<?php

namespace Turahe\Wallet;

use Turahe\Wallet\Commands\RefreshBalance;
use Turahe\Wallet\Interfaces\Mathable;
use Turahe\Wallet\Interfaces\Rateable;
use Turahe\Wallet\Interfaces\Storable;
use Turahe\Wallet\Models\Transaction;
use Turahe\Wallet\Models\Transfer;
use Turahe\Wallet\Models\Wallet;
use Turahe\Wallet\Objects\Bring;
use Turahe\Wallet\Objects\Cart;
use Turahe\Wallet\Objects\EmptyLock;
use Turahe\Wallet\Objects\Operation;
use Turahe\Wallet\Services\CommonService;
use Turahe\Wallet\Services\DbService;
use Turahe\Wallet\Services\ExchangeService;
use Turahe\Wallet\Services\LockService;
use Turahe\Wallet\Services\WalletService;
use Turahe\Wallet\Simple\BrickMath;
use Turahe\Wallet\Simple\Rate;
use Turahe\Wallet\Simple\Store;
use Illuminate\Support\ServiceProvider;

class WalletServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     * @codeCoverageIgnore
     */
    public function boot(): void
    {
        $this->loadTranslationsFrom(
            dirname(__DIR__).'/resources/lang',
            'wallet'
        );

        if (! $this->app->runningInConsole()) {
            return;
        }

        $this->commands([RefreshBalance::class]);

        if ($this->shouldMigrate()) {
            $this->loadMigrationsFrom([
                __DIR__.'/../database/migrations',
            ]);
        }

        if (function_exists('config_path')) {
            $this->publishes([
                dirname(__DIR__).'/config/config.php' => config_path('wallet.php'),
            ], 'laravel-wallet-config');
        }

        $this->publishes([
            dirname(__DIR__).'/database/migrations/' => database_path('migrations'),
        ], 'laravel-wallet-migrations');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(
            dirname(__DIR__).'/config/config.php',
            'wallet'
        );

        // Bind eloquent models to IoC container
        $this->app->singleton(Rateable::class, config('wallet.package.rateable', Rate::class));
        $this->app->singleton(Storable::class, config('wallet.package.storable', Store::class));
        $this->app->singleton(Mathable::class, config('wallet.package.mathable', BrickMath::class));
        $this->app->singleton(DbService::class, config('wallet.services.db', DbService::class));
        $this->app->singleton(ExchangeService::class, config('wallet.services.exchange', ExchangeService::class));
        $this->app->singleton(CommonService::class, config('wallet.services.common', CommonService::class));
        $this->app->singleton(WalletService::class, config('wallet.services.wallet', WalletService::class));
        $this->app->singleton(LockService::class, config('wallet.services.lock', LockService::class));

        // models
        $this->app->bind(Transaction::class, config('wallet.transaction.model', Transaction::class));
        $this->app->bind(Transfer::class, config('wallet.transfer.model', Transfer::class));
        $this->app->bind(Wallet::class, config('wallet.wallet.model', Wallet::class));

        // object's
        $this->app->bind(Bring::class, config('wallet.objects.bring', Bring::class));
        $this->app->bind(Cart::class, config('wallet.objects.cart', Cart::class));
        $this->app->bind(EmptyLock::class, config('wallet.objects.emptyLock', EmptyLock::class));
        $this->app->bind(Operation::class, config('wallet.objects.operation', Operation::class));
    }

    /**
     * Determine if we should register the migrations.
     *
     * @return bool
     */
    protected function shouldMigrate(): bool
    {
        return WalletConfigure::$runsMigrations;
    }
}
