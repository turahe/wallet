<?php

namespace Turahe\Wallet\Traits;

use Turahe\Wallet\Interfaces\Mathable;
use Turahe\Wallet\Interfaces\Storable;
use Turahe\Wallet\Interfaces\Wallet;
use Turahe\Wallet\Models\Transaction;
use Turahe\Wallet\Models\Transfer;
use Turahe\Wallet\Models\Wallet as WalletModel;
use Turahe\Wallet\Services\CommonService;
use Turahe\Wallet\Services\DbService;
use Turahe\Wallet\Services\WalletService;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;

/**
 * Trait HasWallet.
 *
 *
 * @property-read Collection|WalletModel[] $wallets
 * @property-read int $balance
 */
trait HasWallet
{
    use MorphOneWallet;

    /**
     * The input means in the system.
     *
     * @param int|string $amount
     * @param array|null $meta
     * @param bool $confirmed
     *
     * @throws
     * @return Transaction
     */
    public function deposit($amount, ?array $meta = null, bool $confirmed = true): Transaction
    {
        $self = $this;
        return app(DbService::class)->transaction(static function () use ($self, $amount, $meta, $confirmed) {
            return app(CommonService::class)
                ->deposit($self, $amount, $meta, $confirmed);
        });
    }

    /**
     * Magic laravel framework method, makes it
     *  possible to call property balance.
     *
     * Example:
     *  $user1 = User::first()->load('wallet');
     *  $user2 = User::first()->load('wallet');
     *
     * Without static:
     *  var_dump($user1->balance, $user2->balance); // 100 100
     *  $user1->deposit(100);
     *  $user2->deposit(100);
     *  var_dump($user1->balance, $user2->balance); // 200 200
     *
     * With static:
     *  var_dump($user1->balance, $user2->balance); // 100 100
     *  $user1->deposit(100);
     *  var_dump($user1->balance); // 200
     *  $user2->deposit(100);
     *  var_dump($user2->balance); // 300
     *
     * @throws
     * @return int|float|string
     */
    public function getBalanceAttribute()
    {
        return app(Storable::class)->getBalance($this);
    }

    /**
     * all user actions on wallets will be in this method.
     *
     * @return MorphMany
     */
    public function transactions(): MorphMany
    {
        return $this->morphMany(Transaction::class, 'payable');
    }

    /**
     * This method ignores errors that occur when transferring funds.
     *
     * @param Wallet $wallet
     * @param int|string $amount
     * @param array|null $meta
     * @return null|Transfer
     */
    public function safeTransfer(Wallet $wallet, $amount, ?array $meta = null): ?Transfer
    {
        try {
            return $this->transfer($wallet, $amount, $meta);
        } catch (\Throwable $throwable) {
            return null;
        }
    }

    /**
     * A method that transfers funds from host to host.
     *
     * @param Wallet $wallet
     * @param int|string $amount
     * @param array|null $meta
     * @throws
     * @return Transfer
     */
    public function transfer(Wallet $wallet, $amount, ?array $meta = null): Transfer
    {
        app(CommonService::class)->verifyWithdraw($this, $amount);

        return $this->forceTransfer($wallet, $amount, $meta);
    }

    /**
     * Withdrawals from the system.
     *
     * @param int|string $amount
     * @param array|null $meta
     * @param bool $confirmed
     *
     * @return Transaction
     */
    public function withdraw($amount, ?array $meta = null, bool $confirmed = true): Transaction
    {
        app(CommonService::class)->verifyWithdraw($this, $amount);

        return $this->forceWithdraw($amount, $meta, $confirmed);
    }

    /**
     * Checks if you can withdraw funds.
     *
     * @param int|string $amount
     * @param bool $allowZero
     * @return bool
     */
    public function canWithdraw($amount, bool $allowZero = null): bool
    {
        $math = app(Mathable::class);

        /*
         * Allow to buy for free with a negative balance.
         */
        if ($allowZero && ! $math->compare($amount, 0)) {
            return true;
        }

        return $math->compare($this->balance, $amount) >= 0;
    }

    /**
     * Forced to withdraw funds from system.
     *
     * @param int|string $amount
     * @param array|null $meta
     * @param bool $confirmed
     *
     * @throws
     * @return Transaction
     */
    public function forceWithdraw($amount, ?array $meta = null, bool $confirmed = true): Transaction
    {
        $self = $this;

        return app(DbService::class)->transaction(static function () use ($self, $amount, $meta, $confirmed) {
            return app(CommonService::class)
                ->forceWithdraw($self, $amount, $meta, $confirmed);
        });
    }

    /**
     * the forced transfer is needed when the user does not have the money and we drive it.
     * Sometimes you do. Depends on business logic.
     *
     * @param Wallet $wallet
     * @param int|string $amount
     * @param array|null $meta
     * @throws
     * @return Transfer
     */
    public function forceTransfer(Wallet $wallet, $amount, ?array $meta = null): Transfer
    {
        $self = $this;

        return app(DbService::class)->transaction(static function () use ($self, $amount, $wallet, $meta) {
            return app(CommonService::class)
                ->forceTransfer($self, $wallet, $amount, $meta);
        });
    }

    /**
     * the transfer table is used to confirm the payment
     * this method receives all transfers.
     *
     * @return MorphMany
     */
    public function transfers(): MorphMany
    {
        return app(WalletService::class)
            ->getWallet($this, false)
            ->morphMany(Transfer::class, 'from');
    }
}
