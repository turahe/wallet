<?php

namespace Turahe\Wallet\Traits;


use Turahe\Wallet\Models\TransactionPoint;

trait HasPointRoyalty
{
    public function transactionPoints()
    {
        return $this->morphMany(TransactionPoint::class, 'pointable');
    }

    // /**
    //  *
    //  * @return mix
    //  */
    public function averagePoint($round = null)
    {
        if ($round) {
            return $this->transactions()
               ->selectRaw('ROUND(AVG(amount), '.$round.') as averagePointTransaction')
               ->pluck('averagePointTransaction');
        }
        //
        return $this->transactions()
             ->selectRaw('AVG(amount) as averagePointTransaction')
             ->pluck('averagePointTransaction');
    }

    //
    // /**
    //  *
    //  * @return mix
    //  */
    public function countPoint()
    {
        return $this->transactions()
           ->selectRaw('count(amount) as countTransactions')
           ->pluck('countTransactions');
    }

    //
    // /**
    //  *
    //  * @return mix
    //  */
    public function sumPoint()
    {
        return $this->transactions()
             ->selectRaw('SUM(amount) as sumPointTransactions')
             ->pluck('sumPointTransactions');
    }

    //
    // /**
    //  * @param $max
    //  *
    //  * @return mix
    //  */
    public function pointPercent($max = 5)
    {
        $transactions = $this->transactions();
        $quantity = $transactions->count();
        $total = $transactions->selectRaw('SUM(amount) as total')->pluck('total');

        return ($quantity * $max) > 0 ? $total / (($quantity * $max) / 100) : 0;
    }

    /**
     * @return
     */
    public function countTransactions()
    {
        return $this->transactionPoints()
          ->count();
    }

    /**
     * @return float
     */
    public function currentPoints()
    {
        return (new TransactionPoint())->getCurrentPoints($this);
    }

    /**
     * @param $amount
     * @param $message
     * @param $data
     *
     * @return TransactionPoint
     */
    public function addPoints($amount, $message)
    {
        return (new TransactionPoint())->addTransaction($this, $amount, $message);
    }
}
