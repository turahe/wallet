<?php

namespace Turahe\Wallet\Traits;

use Turahe\Wallet\Models\Wallet as WalletModel;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Trait MorphOneWallet.
 * @property-read WalletModel $wallet
 */
trait MorphOneWallet
{
    /**
     * Get default Wallet
     * this method is used for Eager Loading.
     *
     * @return MorphOne
     */
    public function wallet(): MorphOne
    {
        return $this->morphOne(WalletModel::class, 'holder')
            ->where('slug', 'default')
            ->withDefault(array_merge([], [
                'name' => 'Default Wallet',
                'slug' => 'default',
                'meta' => [],
                'balance' => 0,
            ]));
    }
}
