<?php

namespace Turahe\Wallet\Simple;

use Turahe\Wallet\Interfaces\Rateable;
use Turahe\Wallet\Interfaces\Wallet;

/**
 * Class Rate.
 */
class Rate implements Rateable
{
    /**
     * @var string
     */
    protected $amount;

    /**
     * @var Wallet|\Turahe\Wallet\Models\Wallet
     */
    protected $withCurrency;

    /**
     * {@inheritdoc}
     */
    public function withAmount($amount): Rateable
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withCurrency(Wallet $wallet): Rateable
    {
        $this->withCurrency = $wallet;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function convertTo(Wallet $wallet)
    {
        return $this->amount;
    }
}
