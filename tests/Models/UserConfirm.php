<?php

namespace Turahe\Wallet\Test\Models;

use Turahe\Wallet\Interfaces\Confirmable;
use Turahe\Wallet\Interfaces\Wallet;
use Turahe\Wallet\Traits\CanConfirm;
use Turahe\Wallet\Traits\HasWallet;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserConfirm.
 *
 * @property string $name
 * @property string $email
 */
class UserConfirm extends Model implements Wallet, Confirmable
{
    use HasWallet, CanConfirm;

    /**
     * @var array
     */
    protected $fillable = ['name', 'email'];

    /**
     * @return string
     */
    public function getTable(): string
    {
        return 'users';
    }
}
