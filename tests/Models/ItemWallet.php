<?php

namespace Turahe\Wallet\Test\Models;

use Turahe\Wallet\Traits\HasWallets;

class ItemWallet extends Item
{
    use HasWallets;

    /**
     * @return string
     */
    public function getTable(): string
    {
        return 'items';
    }
}
