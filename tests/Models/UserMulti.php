<?php

namespace Turahe\Wallet\Test\Models;

use Turahe\Wallet\Interfaces\Wallet;
use Turahe\Wallet\Interfaces\WalletFloat;
use Turahe\Wallet\Traits\HasWalletFloat;
use Turahe\Wallet\Traits\HasWallets;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User.
 *
 * @property string $name
 * @property string $email
 */
class UserMulti extends Model implements Wallet, WalletFloat
{
    use HasWalletFloat, HasWallets;

    /**
     * @return string
     */
    public function getTable(): string
    {
        return 'users';
    }
}
