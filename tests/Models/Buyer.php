<?php

namespace Turahe\Wallet\Test\Models;

use Turahe\Wallet\Interfaces\Customer;
use Turahe\Wallet\Traits\CanPay;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User.
 *
 * @property string $name
 * @property string $email
 */
class Buyer extends Model implements Customer
{
    use CanPay;

    /**
     * @return string
     */
    public function getTable(): string
    {
        return 'users';
    }
}
