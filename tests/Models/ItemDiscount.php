<?php

namespace Turahe\Wallet\Test\Models;

use Turahe\Wallet\Interfaces\Customer;
use Turahe\Wallet\Interfaces\Discount;
use Turahe\Wallet\Services\WalletService;

class ItemDiscount extends Item implements Discount
{
    /**
     * @return string
     */
    public function getTable(): string
    {
        return 'items';
    }

    /**
     * @param Customer $customer
     * @return int
     */
    public function getPersonalDiscount(Customer $customer): int
    {
        return app(WalletService::class)
            ->getWallet($customer)
            ->holder_id;
    }
}
