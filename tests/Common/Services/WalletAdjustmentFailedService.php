<?php

namespace Turahe\Wallet\Test\Common\Services;

use Turahe\Wallet\Models\Wallet as WalletModel;
use Turahe\Wallet\Services\WalletService;
use Doctrine\DBAL\Exception\InvalidArgumentException;

class WalletAdjustmentFailedService extends WalletService
{
    /**
     * @param WalletModel $wallet
     * @param array|null $meta
     * @throws InvalidArgumentException
     */
    public function adjustment(WalletModel $wallet, ?array $meta = null): void
    {
        throw new InvalidArgumentException(__METHOD__);
    }
}
